<?php

namespace PhpUnitsOfMeasureTest\PhysicalQuantity;

use PhpUnitsOfMeasure\PhysicalQuantity\Velocity;

class VelocityTest extends AbstractPhysicalQuantityTestCase
{
    protected $supportedUnitsWithAliases = [
        'm/s',
        'meters/sec',
        'meters per second',
        'meter per second',
        'metres per second',
        'metre per second',
        'km/h',
        'km/hour',
        'kilometer per hour',
        'kilometers per hour',
        'kilometre per hour',
        'kilometres per hour',
        'ft/s',
        'feet/sec',
        'feet per second',
        'mph',
        'miles/hour',
        'miles per hour',
        'knot',
        'knots',
    ];

    protected function instantiateTestQuantity()
    {
        return new Velocity(1, 'm/s');
    }

    public function testToKilometersPerHour()
    {
        $speed = new Velocity(1, 'km/h');
        $this->assertEquals(0.277778, $speed->toUnit('m/s'));
    }

    public function testToFeetPerSecond()
    {
        $speed = new Velocity(2, 'm/s');
        $this->assertEquals(6.561679790026246, $speed->toUnit('ft/s'));
    }

    public function testToKmPerHour()
    {
        $speed = new Velocity(2, 'mph');
        $this->assertEquals(3.2186854250516594, $speed->toUnit('km/h'));
    }

    public function testToKnot()
    {
        $speed = new Velocity(2, 'm/s');
        $this->assertEquals(3.8876923435786983, $speed->toUnit('knot'));
    }

    public function testMinPerKm()
    {
        $speed = new Velocity(12, 'km/h');
        $this->assertEquals(5, round($speed->toUnit('min/km')));

        $speed = new Velocity(2, 'km/h');
        $this->assertEquals(30, round($speed->toUnit('min/km')));

        $speed = new Velocity(5, 'km/h');
        $this->assertEquals(12, round($speed->toUnit('min/km')));

        $speed = new Velocity(5, 'min/km');
        $this->assertEquals(12, round($speed->toUnit('km/h')));

        $speed = new Velocity(10, 'min/km');
        $this->assertEquals(6, round($speed->toUnit('km/h')));

        $speed = new Velocity(14, 'min/km');
        $this->assertEquals(4.2857, round($speed->toUnit('km/h'), 4));
    }

    public function testMinPerMi()
    {
        $speed = new Velocity(10, 'mph');
        $this->assertEquals(3.7282, round($speed->toUnit('min/km'), 4));


        $speed = new Velocity(20, 'mph');
        $this->assertEquals(1.8641, round($speed->toUnit('min/km'), 4));

        $speed = new Velocity(1, 'mph');
        $this->assertEquals(37.2823, round($speed->toUnit('min/km'), 4));

        $speed = new Velocity(1, 'min/km');
        $this->assertEquals(37.2823, round($speed->toUnit('mph'), 4));

        $speed = new Velocity(10, 'min/km');
        $this->assertEquals(3.7282, round($speed->toUnit('mph'), 4));

        $speed = new Velocity(33, 'min/km');
        $this->assertEquals(1.1298, round($speed->toUnit('mph'), 4));

    }
}
