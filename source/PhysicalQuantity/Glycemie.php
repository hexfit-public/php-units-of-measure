<?php

namespace PhpUnitsOfMeasure\PhysicalQuantity;

use PhpUnitsOfMeasure\AbstractPhysicalQuantity;
use PhpUnitsOfMeasure\UnitOfMeasure;

class Glycemie extends AbstractPhysicalQuantity
{
    protected static $unitDefinitions;

    protected static function initialize()
    {
        // Cubic meter
        $cubicmeter = UnitOfMeasure::nativeUnitFactory('mmol/l');
        static::addUnit($cubicmeter);

        // Cubic millimeter
        $newUnit = UnitOfMeasure::linearUnitFactory('g/l', 5.56);
        static::addUnit($newUnit);

        // Cubic millimeter
        $newUnit = UnitOfMeasure::linearUnitFactory('mg/l', 0.0312);
        static::addUnit($newUnit);
    }
}
